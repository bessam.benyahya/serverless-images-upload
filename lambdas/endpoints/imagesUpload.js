import { _400, _200 } from "../common/API_Responses";
import * as fileType from "file-type";
import { v4 as uuid } from "uuid";
import { Buffer } from "buffer";

import S3 from "../common/S3";

const allowedMimes = ["image/png", "image/jpeg", "image/jpg"];

const bucket = process.env.imagesUploadBucket;
const region = process.env.region;

exports.handler = async (event) => {
  console.log("event", event);

  try {
    const body = JSON.parse(event.body);

    if (!body || !body.image || !body.mime)
      return _400({ message: "Incorrect body on request" });

    if (!allowedMimes.includes(body.mime))
      return _400({ message: "Mime is not correct" });

    let imageData = body.image;
    if (imageData.substring(0, 6) === "base64") {
      imageData = body.image.substring(7, +body.image.length);
    }

    const buffer = Buffer.from(imageData, "base64");
    const fileInfo = await fileType.fromBuffer(buffer);
    const detectedExt = fileInfo.ext;
    const detectedMime = fileInfo.mime;

    if (detectedMime !== body.mime)
      return _400({
        message: "Mime types dont match",
        mimeSend: body.mime,
        mimeDetected: detectedMime,
      });

    const name = uuid();
    const key = `uploads/${name}.${detectedExt}`;

    console.log(`writing image to bucket called ${key}`);

    await S3.write(buffer, key, bucket, null, detectedMime);
    const url = await S3.getSignedURL(bucket, key, 60);

    console.log("signed url", url);

    return _200({ imageURL: url });
  } catch (error) {
    console.log("error", error);
    return _400({ message: "Failed to upload image" });
  }
};
