const AWS = require("aws-sdk");

AWS.config.apiVersions = {
  dynamodb: "2012-08-10",
  // other service API versions
};

const documentClient = new AWS.DynamoDB.DocumentClient();

const Dynamo = {
  async get(ID, TableName) {
    const params = {
      TableName,
      Key: {
        ID,
      },
    };

    const data = await documentClient.get(params).promise();

    if (!data || !data.Item) {
      console.log(data);
      throw Error(
        `There was an error fetching the data for ID of ${ID} from ${TableName}`
      );
    }

    return data.Item;
  },
  async write(data, TableName) {
    if (!data.ID) {
      throw Error("no ID on the data");
    }

    const params = {
      TableName,
      Item: data,
    };

    const response = await documentClient.put(params).promise();

    if (!response) {
      console.log(response);
      throw Error(
        `There was an error inserting ID of ${data.ID} in table ${TableName}`
      );
    }

    return data;
  },
  update: async ({
    tableName,
    primaryKey,
    primaryKeyValue,
    updateKey,
    updateValue,
  }) => {
    const params = {
      TableName: tableName,
      Key: { [primaryKey]: primaryKeyValue },
      UpdateExpression: `set ${updateKey} = :updateValue`,
      ExpressionAttributeValues: {
        ":updateValue": updateValue,
      },
    };

    return documentClient.update(params).promise();
  },
  query: async ({ tableName, index, queryKey, queryValue }) => {
    const params = {
      TableName: tableName,
      IndexName: index,
      KeyConditionExpression: `${queryKey} = :hkey`,
      ExpressionAttributeValues: {
        ":hkey": queryValue,
      },
    };

    const res = await documentClient.query(params).promise();

    return res.Items || [];
  },
};

module.exports = Dynamo;
